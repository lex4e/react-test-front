import React, { Component } from 'react';

class Deployments extends Component {

	constructor(props) {
		super(props);

		this.URL = 'http://localhost:3001';
		this.state = {
			deployments: [],
			url: '',
			templateName: '',
			version: ''
		};

		this.handleUrlChange = this.handleUrlChange.bind(this);
		this.handleTemplateNameChange = this.handleTemplateNameChange.bind(this);
		this.handleVersionChange = this.handleVersionChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}
	
	handleUrlChange(e) {
		this.setState({url: e.target.value});
	}
	
	handleTemplateNameChange(e) {
		this.setState({templateName: e.target.value});
	}
	
	handleVersionChange(e) {
		this.setState({version: e.target.value});
	}
	
	componentDidMount() {
		fetch(`${this.URL}/api/deployments`)
			.then(res => res.json())
			.then(data => {
				this.setState({ deployments: data })
				console.log('STATE', this.state)
			})
			.catch(console.log)
	}
	
	deploymentDelete(object) {
		fetch(`${this.URL}/api/deployment/${object._id}`, {
			method: 'DELETE'
		})
		.then(() => {
			fetch(`${this.URL}/api/deployments`)
			.then(res => res.json())
			.then(data => {
				this.setState({ deployments: data })
			})
			.catch(console.log)
		})
		.catch(console.log)		
	}
	
	handleSubmit(e) {
		e.preventDefault()
		
		console.log(this.state)
		fetch(`${this.URL}/api/deployment`, {
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			body: JSON.stringify({url: this.state.url, templateName: this.state.templateName, version: this.state.version})
		})
		.then(() => {
			fetch(`${this.URL}/api/deployments`)
			.then(res => res.json())
			.then(data => {
				this.setState({ deployments: data, url: '', templateName: '', version: '' })
			})
			.catch(console.log)
		})
		.catch(console.log)
	}
	
	render() {
		return (
			<div>
				<h1>Deployments</h1>
				<table className='table'>
					<thead><tr><td>ID</td><td>URL</td><td>Template Name</td><td>Version</td><td>Created</td></tr></thead>
					<tbody>
					{this.state.deployments ? this.state.deployments.map(deployment => {
						return (<tr>
							<td>{deployment._id}</td>
							<td>{deployment.url}</td>
							<td>{deployment.templateName}</td>
							<td>{deployment.version}</td>
							<td>{deployment.deployedAt}</td>
							<td><button onClick={() => this.deploymentDelete(deployment)} className='btn btn-danger'>delete</button></td>
						</tr>)
					}) : ''}
					</tbody>
				</table>
				<h2>Add Deployment</h2>
				<form className='form' onSubmit={this.handleSubmit}>
					<div className='form-group'><input className='form-control' placeholder="url" type="text" value={this.state.url} onChange={this.handleUrlChange} /></div>
					<div className='form-group'><input className='form-control' placeholder="templateName" type="text" value={this.state.templateName} onChange={this.handleTemplateNameChange} /></div>
					<div className='form-group'><input className='form-control' placeholder="version" type="text" value={this.state.version} onChange={this.handleVersionChange} /></div>
					<div className='form-group'><button className='btn btn-success'>Create</button></div>
				</form>
			</div>
		)
	}
}

export default Deployments