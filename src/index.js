import React from 'react'
import ReactDOM from 'react-dom'
import Deployments from './Deployments'

ReactDOM
	.render(<Deployments />, document.getElementById('root'))
